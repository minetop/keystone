package fr.minetop.Keystone;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import fr.minetop.Keystone.database.Database;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Keystone extends JavaPlugin {

    Database database;
    
    private static List<Player> loggedInPlayers = new ArrayList<Player>();

    public void onEnable() {
        this.database = new Database();
    }

    public boolean isLoggedIn(Player player) {
        return loggedInPlayers.contains(player);
    }

    public void logIn(Player player, String password) {
        if (checkPassword(player, password)) {
            loggedInPlayers.add(player);
        }
    }

    public void logOut(Player player) {
        if (!isLoggedIn(player)) return;
    }

    public void setPassword(Player player, String newPassword) {

    }

    public boolean checkPassword(Player player, String password) {
        return false;
    }

    private String getPasswordHash(Player player) {
        return null;
    }

}