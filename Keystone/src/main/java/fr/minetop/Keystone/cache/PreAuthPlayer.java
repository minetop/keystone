package fr.minetop.Keystone.cache;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

public class PreAuthPlayer {
	
	private Location location;
	private GameMode gameMode;
	
	private ItemStack[] inventory;
	private ItemStack[] armor;
	
	private boolean isOperator = false;
	private boolean isFlying = false;
	
	public Location getLocation() {
		return this.location;
	}
	
	public GameMode getGameMode() {
		return this.gameMode;
	}
	
	public ItemStack[] getInventory() {
		return this.inventory;
	}
	
	public ItemStack[] getArmorContents() {
		return this.armor;
	}
	
	public boolean isOperator() {
		return this.isOperator;
	}
	
	public boolean isFlying() {
		return this.isFlying;
	}
	
}