package fr.minetop.Keystone.database;

import fr.minetop.Keystone.database.drivers.DatabaseType;

public class AuthDatabase extends Database {
	
	public static String TABLE_NAME = "accounts";
	
	public AuthDatabase(DatabaseType driver) {
		super(driver);
	}
	
	public void createTable() {
		this.updateQuery("CREATE TABLE IF NOT EXISTS ? (uuid VARCHAR(64) UNIQUE NOT NULL PRIMARY KEY, password VARCHAR(256));", TABLE_NAME);
	}
	
	public void createUser() {
		
	}
	
}