package fr.minetop.Keystone.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import fr.minetop.Keystone.database.drivers.DatabaseType;
import fr.minetop.utils.runnable.Callback;
import fr.minetop.utils.runnable.RunnableCallback;
import com.jolbox.bonecp.BoneCP;

public class Database {
	
	private BoneCP boneCP;

	public Database(DatabaseType driver) {
		try {
			this.boneCP = new BoneCP(driver.getConfig());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private BoneCP getBoneCP() {
		return this.boneCP;
	}
	
    public Connection getConnection() {
        try {
            return this.getBoneCP().getConnection();
        } catch (SQLException e) {
            System.out.println("Error whilst getting the connection: " + e.getMessage());
            return null;
        }
    }

	public void updateQuery(final String query, final Object... parameters) {
		Executor executor = Executors.newSingleThreadExecutor();
		executor.execute(new Runnable() {
			public void run() {
			    Connection con = null;
			    PreparedStatement pst = null;
			    
				try {
					con = getConnection();
					pst = con.prepareStatement(query);
					
					for (int i=0; i<parameters.length; i++) { 
						pst.setObject(i, parameters[i]);
					}
					
					pst.executeUpdate();
				}
				catch (SQLException e) {
					System.out.println("Error whilst executing query: " + e.getMessage());
					e.printStackTrace();
				}
				finally {
					if (con != null) {
						try { con.close(); } catch (SQLException e) { }
					}
				}
			}
		});
	}
	
	public void query(final String query, final Callback callback, final Object... parameters) {
		Executor executor = Executors.newSingleThreadExecutor();
		executor.execute(new RunnableCallback(callback) {
			public void run() {
			    Connection con = null;
			    PreparedStatement pst = null;
			    
				try {
					con = getConnection();
					pst = con.prepareStatement(query);
					
					for (int i=0; i<parameters.length; i++) { 
						pst.setObject(i, parameters[i]);
					}
					
					ResultSet rs = pst.executeQuery();
				}
				catch (SQLException e) {
					System.out.println("Error whilst executing query: " + e.getMessage());
					e.printStackTrace();
				}
				finally {
					if (con != null) {
						try { con.close(); } catch (SQLException e) { }
					}
				}
			}
		});
	}
  
}