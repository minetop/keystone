package fr.minetop.Keystone.database.drivers;

import com.jolbox.bonecp.BoneCPConfig;

public abstract class DatabaseType {

	private BoneCPConfig config;
    
    public DatabaseType() {    	
    	this.config = new BoneCPConfig();
    }
    
    public BoneCPConfig getConfig() {
    	return this.config;
    }
    
    public abstract String getDriverClass();

}