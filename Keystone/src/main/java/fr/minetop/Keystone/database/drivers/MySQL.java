package fr.minetop.Keystone.database.drivers;

public class MySQL extends DatabaseType {

	public MySQL(String host, String database, String username, String password) {
		super();
		
		this.getConfig().setJdbcUrl("jdbc:mysql://"+host+"/"+database);
		this.getConfig().setUsername(username);
		this.getConfig().setPassword(password);
	}

	public String getDriverClass() {
		return "com.mysql.jdbc.Driver";
	}

}