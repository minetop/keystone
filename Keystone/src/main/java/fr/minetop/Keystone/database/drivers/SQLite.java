package fr.minetop.Keystone.database.drivers;

import java.io.File;

public class SQLite extends DatabaseType {

	public SQLite(File folder, String database) {
		super();	
		
    	this.getConfig().setJdbcUrl("jdbc:sqlite:"+folder+File.separator+database);
	}
	
	public String getDriverClass() {
		return "org.sqlite.JDBC";
	}

}