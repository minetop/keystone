package fr.minetop.Keystone.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import fr.minetop.Keystone.Keystone;;

public class PlayerListener implements Listener {

    private void proceedEvent(Player player, Cancellable event) {
        if (!Keystone.isLoggedIn(player)) {
            event.setCancelled(true);
        }
    }

    public void onPlayerMove(PlayerMoveEvent event) {
        this.proceedEvent(event.getPlayer(), event);
    }

    public void onPlayerInteract(PlayerInteractEvent event) {
        this.proceedEvent(event.getPlayer(), event);
    }

    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        this.proceedEvent(event.getPlayer(), event);
    }

    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
        if (!event.getMessage().startsWith("/login")) {
            this.proceedEvent(event.getPlayer(), event);
        }
    }

}