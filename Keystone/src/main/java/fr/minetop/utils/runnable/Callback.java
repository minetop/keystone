package fr.minetop.utils.runnable;

public interface Callback {
	
	public void run();
	
}