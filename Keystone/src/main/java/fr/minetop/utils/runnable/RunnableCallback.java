package fr.minetop.utils.runnable;

public class RunnableCallback implements Runnable {

	Callback callback;
	
	public RunnableCallback(Callback callback) {
		this.callback = callback;
	}
	
	public void run() {
		callback.run();
	}

}